# ElementTree改进

## 原有库缺点

- 无法获取一个elem的parent
- 无法指定iter()方法，只能顺序深度遍历

## 改进

- 可以通过`getparent()`或者`elem.parent`方法获取父节点
- 增加如下三个方法，以便更灵活的调用
    1. `breadthiter`:广度顺序遍历elem下所有符合tag的子节点
    1. `reverseiter`:深度倒序遍历elem下所有符合tag的子节点
    1. `reversebreadthiter`：广度倒序遍历elem下所有符合tag的子节点

## How to use

覆盖python库中`xml/etree/ElementTree.py`文件